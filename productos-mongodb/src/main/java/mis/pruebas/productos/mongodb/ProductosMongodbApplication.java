package mis.pruebas.productos.mongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductosMongodbApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductosMongodbApplication.class, args);
	}

}
