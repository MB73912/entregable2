package mis.pruebas.productos.mongodb.controlador;

import mis.pruebas.productos.mongodb.modelo.Producto;
import mis.pruebas.productos.mongodb.servicio.ServicioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/almacen/v2/productos")
public class ControladorProducto {
    @Autowired
    ServicioProducto servicioProducto;

    //POST
    @PostMapping
    public void guardarProducto(@RequestBody Producto p){
        System.out.println(String.format("=====Paso1"));
        p.setId(null);
        this.servicioProducto.agregar(p);
    }

    //GET inicial - buscar todos los elementos
    /*@GetMapping
    public List<Producto> obtenerProductos(){
        return this.servicioProducto.obtenerTodos();
    }*/

    //GET por parametros
    @GetMapping
    public List<Producto> obtenerProductos(@RequestParam(required = false) Double maxPrecio,
                                           @RequestParam(required = false) String nombreProveedor){
        if (nombreProveedor !=null){return this.servicioProducto.buscarPorProveedor(nombreProveedor);}
        if (maxPrecio !=null){return this.servicioProducto.buscarPorRangoPrecio(0.0, maxPrecio.doubleValue());}
        return this.servicioProducto.obtenerTodos();
    }

    public static class Precio {
        public double precio;
    }

    //PATCH un campo
    @PatchMapping("/{id}")
    public void cambiarPrecio (@PathVariable String id,@RequestBody Precio pp){
        this.servicioProducto.corregirPrecio(id, pp.precio);
    }

    //DEL
    @DeleteMapping("/{id}")
    public void eliminarProducto(@PathVariable String id){
        this.servicioProducto.borrar(id);
    }

}
