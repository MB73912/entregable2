package mis.pruebas.productos.mongodb.servicio.impl;

import mis.pruebas.productos.mongodb.modelo.Producto;
import mis.pruebas.productos.mongodb.modelo.Proveedor;
import mis.pruebas.productos.mongodb.servicio.RepositorioProducto;
import mis.pruebas.productos.mongodb.servicio.ServicioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServicioProductoImpl implements ServicioProducto {

    @Autowired
    RepositorioProducto repositorioProducto;

    @Override
    public void agregar(Producto p){
        p.setId(null);
        this.repositorioProducto.insert(p);
    }

    @Override
    public List<Producto> buscarPorRangoPrecio (double minP, double maxP){
        return this.repositorioProducto.buscarPorRangoPrecio(minP, maxP);
    }

    @Override
    public List<Producto> obtenerTodos(){return this.repositorioProducto.findAll();}

    @Override
    public void borrar(String id){
        this.repositorioProducto.deleteById(id);
    }

    @Override
    public void corregirPrecio(String id, double precio){
        this.repositorioProducto.cambiarPrecio(id, precio);
    }

    @Override
    public List<Producto> buscarPorProveedor (String nombreProveedor){
        return this.repositorioProducto.buscarPorProveedor(nombreProveedor);

    }
}
