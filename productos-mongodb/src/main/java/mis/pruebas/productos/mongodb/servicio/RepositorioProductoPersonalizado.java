package mis.pruebas.productos.mongodb.servicio;

import mis.pruebas.productos.mongodb.modelo.Producto;

import java.util.List;

public interface RepositorioProductoPersonalizado{
    public void cambiarPrecio(String id, double precio);
    public List<Producto> buscarPorRangoPrecioPersonalizado(double min, double max);
}
