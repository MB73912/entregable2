package mis.pruebas.productos.mongodb.servicio;

import mis.pruebas.productos.mongodb.modelo.Producto;

import java.util.List;

public interface ServicioProducto {
    public void agregar(Producto p);
    public List<Producto> obtenerTodos();
    public List<Producto> buscarPorRangoPrecio (double minP, double maxP);
    public List<Producto> buscarPorProveedor (String nombreProveedor);
    public void borrar(String id);
    public void corregirPrecio(String id,double precio);
}
