package mis.pruebas.productos.mongodb.servicio.impl;


import com.mongodb.client.result.UpdateResult;
import mis.pruebas.productos.mongodb.modelo.Producto;
import mis.pruebas.productos.mongodb.servicio.RepositorioProductoPersonalizado;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.List;


public class RepositorioProductoPersonalizadoImpl implements RepositorioProductoPersonalizado {
    @Autowired
    MongoOperations mongoOperations;

    @Override
    public void cambiarPrecio(String id, double precio){
        final Query q = new Query();
        q.addCriteria(Criteria.where("_id").is(new ObjectId(id)));
        final Update u = new Update();
        u.set("precio",precio);
        final UpdateResult r =this.mongoOperations.updateFirst(q, u, Producto.class);
    }

    @Override
    public List<Producto> buscarPorRangoPrecioPersonalizado(double min, double max) {
        Query q = new Query();
        q.addCriteria(Criteria.where("precio").lte(max));
        return this.mongoOperations.find(q,Producto.class);
    }

}
